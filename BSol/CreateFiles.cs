﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BSol
{
    class CreateFiles
    {
        private Queue<Structures.TradeRecord> queueTR = new Queue<Structures.TradeRecord>();
        private Queue<Structures.Tick> queueTick = new Queue<Structures.Tick>();
        bool flag = true;
        bool CSVend = false;
        bool BINend = false;
        private int j = 3000000;

        public void Create()
        {
            Console.WriteLine("Create....");

            // Лишние лямбды. 
            // можно переписать проще
            // Thread treadCSV = new Thread(CreateCSV); 
            Thread treadCSV = new Thread(() =>
            {
                CreateCSV();
            });
            

            Thread treadBin = new Thread(() =>
            {
                CreateBin();
            });

            Thread treadList = new Thread(() =>
            {
                CreateList();
            });

            treadCSV.Start();
            treadBin.Start();
            treadList.Start();
        }

        private void CreateCSV()
        {
            // Хардкод пути к файлу, как минимум стоит вынести в константную переменную. 
            // В идеале передавать в объект из вне через конструктор или какой-нибудь метод
            using (var sw = new StreamWriter(@"D:\Work\New folder\CSV\pr\temp.csv", false, Encoding.Default))
            {
                bool flagCSV = true;
                int count = 0;
                while (flagCSV)
                {
                    if (queueTick.Count > 0)
                    {
                        Structures.Tick objT;

                        lock (queueTick)
                        {
                            objT = queueTick.Dequeue();
                            count++;
                        }

                        sw.Write("{0};", (uint)objT.id);
                        sw.Write("{0};", (uint)objT.account);
                        sw.Write("{0};", (double)objT.volume);
                        sw.Write("{0};", (string)objT.comment);
                        sw.WriteLine();

                        if (count == j && !flag)
                        {
                            flagCSV = false;
                            CSVend = true;
                        }
                    }                    
                }
            }
        }

        private void CreateBin()
        {
            // Тут тоже самое про хардкод
            using (var fs = new FileStream(@"D:\Work\New folder\CSV\pr\temp.bin", FileMode.Create))
            using (var bw = new BinaryWriter(fs, Encoding.Default))
            {
                bool flagBin = true;
                while (flagBin)
                {
                    if (queueTR.Count != 0)
                    {
                        Structures.TradeRecord objTR;

                        lock (queueTR)
                        {
                            objTR = queueTR.Dequeue();
                        }

                        bw.Write(Structures.RawSerialize(objTR));
                    }
                    if (!flag && queueTR.Count == 0)
                    {
                        flagBin = false;
                        BINend = true;

                    }
                }
            }
        }

        private void CreateList()
        {
            var rnd = new Random();
            int count = 0;
            while(count < j)
            {
                // тут как минимум инфертация if уберёт уровеь вложенности
                // + тут можно вынести как минимум 2 дополнительных метода
                if(queueTick.Count < 5 && queueTR.Count < 5)
                {
                    count++;

                    int id = rnd.Next();
                    int account = rnd.Next();
                    double volume = rnd.NextDouble();
                    string comment = rnd.ToString();

                    Structures.TradeRecord tr = new Structures.TradeRecord();
                    tr.id = id;
                    tr.account = account;
                    tr.volume = volume;
                    tr.comment = comment;

                    lock (queueTR)
                    {
                        queueTR.Enqueue(tr);
                    }

                    Structures.Tick tick = new Structures.Tick();
                    tick.id = id;
                    tick.account = account;
                    tick.volume = volume;
                    tick.comment = comment;

                    lock (queueTick)
                    {
                        queueTick.Enqueue(tick);
                    }

                    if (count == (j - 1))
                    {
                        flag = false;
                    }
                }

            }
        }
    }
}
