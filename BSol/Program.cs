﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSol
{
    class Program
    {
        static Conversion conv = new Conversion();

        static void Main(string[] args)
        {
            bool flag = true;

            string menu = "If you want run covrsion, you must write 'conv'. \nIf you want create test file, you must write 'create'. \nYou can watch progress conversion file, you must write 'pr'.";
            Console.WriteLine(menu);
            string end = "If you want end application, you ,ust write 'end'.";
            Console.WriteLine(end);

            // Этот блок напрашивается на вынесение в отдельную фнкцию
            // + опечатки в словах
            while (flag)
            {
                string selection = Console.ReadLine();
                switch (selection)
                {
                    case "conv":
                        {
                            Console.WriteLine("Start covrsion");
                            StartConv();
                        };
                        break;
                    case "create":
                        {
                            Console.WriteLine("Start create");
                            StartCreate();
                        };
                        break;
                    case "pr":
                        {
                            conv.ProgressConv();
                        };
                        break;
                    case "end":
                        {
                            flag = false;
                        };
                        break;
                }
            }
        }

        static void StartConv()
        {
            Console.WriteLine("Enter the path to the file.bin");
            string pathOld = Console.ReadLine();

            Console.WriteLine("Enter the path to the new file CSV");
            string pathNew = Console.ReadLine();

            conv.StartConversion(pathOld, pathNew);            
        }

        // Тут плохой нейминг? 
        // Почему тут называется Start.. если ни чего не стартует по сути? 
        static void StartCreate()
        {
            CreateFiles create = new CreateFiles();
            create.Create();
        }
    }
}
