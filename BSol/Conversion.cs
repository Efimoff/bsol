﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;

namespace BSol
{
    class Conversion
    {
        // Опечатка
        private int BufferLenght;

        private string destinationFile;
        private string sourceFile;
        private bool flag;
        private double progress = 0;

        private Queue<Structures.TradeRecord> queue = new Queue<Structures.TradeRecord>();

        private double countBlocks = 0;
        private int numReads = 0;

        public Conversion()
        {
            BufferLenght = Marshal.SizeOf(typeof(Structures.TradeRecord));            
        }

        public void StartConversion(string source, string destination)
        {

            destinationFile = destination;
            sourceFile = source;
            flag = true;

            Console.WriteLine("Conversion....");

            // Так же можно упростить. Лишние лямбды
            Thread treadRead = new Thread(() =>
            {
                ReadFile();
            });
            Thread treadWrite = new Thread(() =>
            {
                RecordFile();
            });
            treadRead.Start();
            treadWrite.Start();
        }

        private void ReadFile()
        {
            using (var sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
            {
                double sLength = sourceStream.Length;
                countBlocks = Math.Ceiling(sLength / BufferLenght);
                bool localRunRead = flag;
                while (localRunRead)
                {
                    localRunRead = flag;
                    if (queue.Count < 4)
                    {
                        numReads++;

                        Structures.TradeRecord newBlock = (Structures.TradeRecord)Structures.ReadStruct(sourceStream, typeof(Structures.TradeRecord));

                        lock (queue)
                        {
                            queue.Enqueue(newBlock);
                        }                        
                    }
                    if (countBlocks == numReads)
                    {
                        Console.WriteLine("All blocks were read");
                        localRunRead = false;
                    }
                }
            }
            numReads = 0;
            countBlocks = 0;
        }

        private void RecordFile()
        {
            // Кривой нейминг. 
            // Название и тип переменной destinationStream противоречат немного
            // Почему не назвать просто writer? 
            using (StreamWriter destinationStream = new StreamWriter(destinationFile, false, Encoding.Default))
            {
                bool localRunWrite = flag;
                while (localRunWrite)
                {
                    if (queue.Count != 0)
                    {
                        Structures.TradeRecord readBlock;
                        lock (queue)
                        {
                            readBlock = queue.Dequeue();
                        }

                        // Тут тоже хочется вынести отдельный метод
                        // Как вариант чтоб было что-то вроде
                        // readBlock.WriteToStream(writer);
                        destinationStream.Write(readBlock.id + ";");
                        destinationStream.Write(readBlock.account + ";");
                        destinationStream.Write(readBlock.volume + ";");
                        destinationStream.Write(readBlock.comment + ";\n");

                        if (numReads == countBlocks && queue.Count == 0)
                        {
                            Console.WriteLine("All blocks were recorded");
                            localRunWrite = false;
                            flag = false;
                            progress = 0;
                        }
                    }
                }
            }
        }

        public void ProgressConv()
        {
            // Тут не сильно опасно, но может быть гонка за данные
            progress = Math.Round((numReads / countBlocks * 100), 2);
            Console.WriteLine("Progress conversion: {0}%", progress);
        }
    }
}
